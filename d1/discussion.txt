CLI (Command Line Interface) Commands

ls - (list) contains the files and folders contained by the current directory.

pwd - (print working directory/present working directory) - shows the current folder we are currently working on. 

cd <folderName/path to folder> - (changes our directory) change the current folder/directory  we are currently working on our CLI (terminal/gitbash)

mkdir <folderName> - create a new directory

Sublime Text 4
>>lightweight text/file editor
>>uses less RAM/memory which is important because we also, as web developers, have our Google Chrome open.

Configure our Git

git config --global user.email "philjosephmendoza.is@gmail.com"
--this allows us to identify the account from gitlab or github who will push/upload files into our online gitlab/github services.

git config --global user.name "pjdmendoza"
--this will also allow us to determine the name of the person who uploaded the latest commit/version in our repository.

Pushing for the very first time

git init
--initialize a local folder as local git repository.
--means that the folder and its files are now tracked by git

git add .
--allows us to add all files and updates into a new commit/version of our files and folders to be uploaded to our online repository.

git commit -m "<commitMessage>"
--allows us to create a new version of our files and folders based on the updates added using git add .

git remote add origin <gitUrlOnlineRepo>
--this allows us to connect an online repository to a local repo
--"origin" is the default or conventional name for an online repo

git push origin master
--allows us to push/upload the lastest commit created into our online repository that is designated as "origin"
--master is the default version or the main version of our files in the repository





